package com.nisum.msauth.adapters.jpa.entities

import com.nisum.msauth.domain.Role
import jakarta.persistence.*
import lombok.AllArgsConstructor
import lombok.Data
import lombok.NoArgsConstructor
import org.hibernate.annotations.ManyToAny

@Entity @Data @NoArgsConstructor @AllArgsConstructor
class User(
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    val id: Long = 0,
    val name: String? = null,
    val userName: String? = null,
    val password: String? = null,
    @ManyToAny(fetch = FetchType.EAGER)
    val roles: Collection<Role> = arrayListOf()
)