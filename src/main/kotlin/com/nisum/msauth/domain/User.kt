package com.nisum.msauth.domain

import lombok.AccessLevel
import lombok.AllArgsConstructor
import lombok.Builder
import lombok.Value

@Value @Builder @AllArgsConstructor(access = AccessLevel.PRIVATE)
class User(
    val id: Long = 0,
    val name: String? = null,
    val userName: String? = null,
    val password: String? = null,
    val roles: Collection<Role> = arrayListOf()
)